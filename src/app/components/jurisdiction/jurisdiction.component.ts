import { Component, OnInit,ViewChild,OnDestroy } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {MatAccordion} from '@angular/material/expansion';
import { MarketValueService } from './../../services/marketvalue.service';
import { Params } from './../../model/Params';
import { Form } from './../../model/Form';
import { HttpParams } from '@angular/common/http';
import { formatCurrency,getCurrencySymbol } from "@angular/common";
import {Router} from '@angular/router';

@Component({
  selector: 'app-jurisdiction',
  templateUrl: './jurisdiction.component.html',
  styleUrls: ['./jurisdiction.component.css']
})
export class JurisdictionComponent implements OnInit,OnDestroy {

  title1 = "Jurisdiction Info";
  title2 = "Land Type";
  title3 = "Plot Location";
  notFound = "No Match";

   /** control for the selected district */
  public districtCtrl: FormControl = new FormControl(Validators.required);

   /** control for the selected police station */
  public policeStationCtrl: FormControl = new FormControl(Validators.required);

     /** control for the selected mouza */
  public mouzaCtrl: FormControl = new FormControl(Validators.required);

  public plotNo: FormControl = new FormControl(Validators.required);

  public plotType: FormControl = new FormControl({value : 'RS'});

  public bataNo: FormControl = new FormControl();

  public khatianNo: FormControl = new FormControl();

  public areaType: FormControl = new FormControl();

  public currLandUse: FormControl = new FormControl();

  public basePriceCtrl: FormControl = new FormControl({value : '', disabled : true});

  public changeLandUseType: FormControl = new FormControl();

  public newLandType: FormControl = new FormControl({value:''});

  public areaOfPlot: FormControl = new FormControl({value : '', disabled : true},Validators.required);

  public basePriceMFCtrl: FormControl = new FormControl({value : '', disabled : true});

  public fetchType: FormControl = new FormControl({value : 'Manually'});

  public isPlotOnRoad: FormControl = new FormControl();

  public distanceFromRoad: FormControl = new FormControl();

  public widthOfApproachRoad: FormControl = new FormControl({value : '', disabled : true});

  public distanceFromMarket: FormControl = new FormControl();

  public areaUnit: FormControl = new FormControl();

  public marketValueCtrl: FormControl = new FormControl({value : '', disabled : true});

  public areaUnits = ['Decimal','Acre','Bigha','Katha','Chatak','Sq Feet'];

  public distanceParameters : any;

  public widthParameters : any;

  public marketParameters : any;

  public showBasePrice : boolean = false;

  public showBasePriceMF : boolean = false;

  public showMarketValue : boolean = false;

  public basePriceMandatoryNotFilled : boolean = false;

  public basePriceMandatoryNotFilledHint : string;

  public basePriceMFMandatoryNotFilled : boolean = false;

  public basePriceMFMandatoryNotFilledHint : string;

  public marketValueMandatoryNotFilled : boolean = false;

  public marketValueMandatoryNotFilledHint : string;

  public error : boolean = false;

  public errorMessage : string;

  public success : boolean = false;

  public successMessage : string;

  public prevError: boolean = false;

  public prevErrorMessage: string;

  public marketValuePerDecimal: number;

  public autoComplete : boolean = false;

  public jurisdictionForm = new FormGroup({
  jurisdictionInfo: new FormGroup({
    district : this.districtCtrl,
    policeStation:this.policeStationCtrl,
    mouza:this.mouzaCtrl
  }),
  subJurisdiction: new FormGroup({
    plotType: this.plotType,
    plotNo: this.plotNo,
    bataNo: this.bataNo,
    khatianNo: this.khatianNo,
    areaType: this.areaType,
    currLandUse: this.currLandUse,
    basePrice: this.basePriceCtrl
  }),
  landType : new FormGroup({
    changeLandUseType: this.changeLandUseType,
    newLandType: this.newLandType,
    areaOfPlot: this.areaOfPlot,
    areaUnit: this.areaUnit,
    basePriceMF : this.basePriceMFCtrl
  }),
  plotLocation : new FormGroup({
    fetchType: this.fetchType,
    isPlotOnRoad: this.isPlotOnRoad,
    distanceFromRoad: this.distanceFromRoad,
    widthOfApproachRoad:this.widthOfApproachRoad,
    distanceFromMarket: this.distanceFromMarket,
    marketValue: this.marketValueCtrl
  })
});

public params: Params;
districts: any;
policeStations: any;
mouzas: any;
areaTypes: any;
landTypes: any;
currentLandUse: any;
basePrice: any;
basePriceWithMF: any;
marketPrice: any;
prevMarketVal: any;
landName: string = '';

  /** control for the MatSelect filter keyword */
  public policeStationFilterCtrl: FormControl = new FormControl();

  /** control for the MatSelect filter keyword */
  public districtFilterCtrl: FormControl = new FormControl();

  /** control for the MatSelect filter keyword */
  public mouzaFilterCtrl: FormControl = new FormControl();

  public newLandTypeFilterCtrl: FormControl = new FormControl();

  protected _onDestroy = new Subject<void>();

  public filteredDistricts: ReplaySubject<Params[]> = new ReplaySubject<Params[]>(1);

  public filteredPoliceStation: ReplaySubject<Params[]> = new ReplaySubject<Params[]>(1);

  public filteredMouza: ReplaySubject<Params[]> = new ReplaySubject<Params[]>(1);

  public filteredNewLandType: ReplaySubject<Params[]> = new ReplaySubject<Params[]>(1);

  @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;

  constructor(private marketValueService: MarketValueService,private router: Router) {
   }

  ngOnInit(): void {

    this.loadDistricts();

        // listen for search field value changes
        this.districtFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
          this.filterDistricts();

    });

    // listen for search field value changes
    this.policeStationFilterCtrl.valueChanges
    .pipe(takeUntil(this._onDestroy))
    .subscribe(() => {
      this.filterpoliceStations();

  });

  // listen for search field value changes
  this.mouzaFilterCtrl.valueChanges
  .pipe(takeUntil(this._onDestroy))
  .subscribe(() => {
    this.filterMouza();

  });

  // listen for search field value changes
  this.newLandTypeFilterCtrl.valueChanges
  .pipe(takeUntil(this._onDestroy))
  .subscribe(() => {
    this.filterNewLandType();

  });

  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  loadDistricts(){
    this.marketValueService.getMethodCall('districts',null).subscribe(
      res => {
        this.error = false;
        this.districts = res ;
        this.districtCtrl.setValue(this.districts);
        this.filteredDistricts.next(this.districts.slice());
      },
      err => {
        this.error = true;
        this.errorMessage = 'District information cannot be loaded';
      }
    );
  }


  public loadMouzaDetails()
  {
    let httpParams = new HttpParams().set('district',this.districtCtrl.value.districtName).set('policeStation',this.policeStationCtrl.value.psName);
    this.marketValueService.getMethodCall('mouza',httpParams).subscribe(
      res => {
        this.error = false;
        this.mouzas =res ;
        this.mouzaCtrl.setValue(this.mouzas);
        this.filteredMouza.next(this.mouzas.slice());
      },
      err => {
        this.error = true;
        this.errorMessage = 'Mouza information is not available for corresponding user selection';
      }
    );
  }

  public loadPSDetails()
  {
    let httpParams = new HttpParams().set('district',this.districtCtrl.value.districtName);
    this.marketValueService.getMethodCall('policeStations',httpParams).subscribe(
      res => {
        this.error = false;
        this.policeStations =res ;
        this.policeStationCtrl.setValue(this.policeStations);
        this.filteredPoliceStation.next(this.policeStations.slice());
      },
      err => {
        this.error = true;
        this.errorMessage = 'Police Station information cannot be loaded';
      }
    );
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    
  }

  protected filterDistricts() {
    if (!this.districts) {
      return;
    }
    // get the search keyword
    let search = this.districtFilterCtrl.value;
    if (!search) {
      this.filteredDistricts.next(this.districts.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the district
    this.filteredDistricts.next(
      this.districts.filter(district => district.district.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterpoliceStations() {
    if (!this.policeStations) {
      return;
    }
    // get the search keyword
    let search = this.policeStationFilterCtrl.value;
    if (!search) {
      this.filteredPoliceStation.next(this.policeStations.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the police station
    this.filteredPoliceStation.next(
      this.policeStations.filter(ps => ps.psName.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterMouza() {
    if (!this.mouzas) {
      return;
    }
    // get the search keyword
    let search = this.mouzaFilterCtrl.value;
    if (!search) {
      this.filteredMouza.next(this.mouzas.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the mouza
    this.filteredMouza.next(
      this.mouzas.filter(mouza => mouza.mouzaName.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterNewLandType() {
    if (!this.landTypes) {
      return;
    }

    // get the search keyword
    let search = this.newLandTypeFilterCtrl.value;
    if (!search) {
      this.filteredNewLandType.next(this.landTypes.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the new land types
      this.filteredNewLandType.next(
      this.landTypes.filter(land => land.landName.toLowerCase().indexOf(search) > -1)
    );
  }

  onChangeMouza()
  {
    this.basePriceMFMandatoryNotFilledHint = '';
    this.basePriceMandatoryNotFilledHint = '';
    this.jurisdictionForm.get('landType').reset();
    this.jurisdictionForm.get('plotLocation').reset();
    this.setAreaType();
    this.setCurrentLandUse();
    this.getBasePrice();
    this.loadDistanceParameters();
    this.loadMarketParameters();
    this.loadWidthParameters();
  }

  setAreaType(){
    this.areaType.setValue(this.mouzaCtrl.value.areaType);
  }

  loadLandTypes(){
    this.marketValueService.getMethodCall('getLandTypes',null).subscribe(
      res => {
        this.error = false;
        this.landTypes=res ;
        this.newLandType.setValue(this.landTypes);
        this.filteredNewLandType.next(this.landTypes.slice());
      },
      err => {
        this.error= true;
        this.errorMessage = 'Land information is not available corresponding for user selection';
      }
    );
  }

  onChangePlotNo(){
    this.setCurrentLandUse();
    this.populatePlotParameter();
}

populatePlotParameter()
{
  if(this.districtCtrl.dirty && this.policeStationCtrl.dirty && this.mouzaCtrl.dirty
    && this.plotNo.dirty)
  {
    this. error = false;
  this.params = new Params(this.districtCtrl.value.districtName,
    this.policeStationCtrl.value.psName,this.mouzaCtrl.value.mouzaName,this.plotNo.value);
  this.marketValueService.postMethodCall('getPlotDistParameters',this.params).subscribe(
    res => {
      
      let result : any = res;
      let roadDist : any;
      let marketDist: any;
      let isAdjToRoad = 'No';
      if(result)
      {
      for(let i=0;i<result.length;i++)
      {
          let param = result[i];
          if(param && param.paramType && param.paramType.includes('MR'))
          {
             roadDist = param;
          }
          if(param && param.paramType && param.paramType.includes('MM'))
          {
            marketDist = param;
          }
          if(param.paramType=='MM0' || param.paramType=='MR0')
          {
             isAdjToRoad = 'Yes';
          }
      }

          if(roadDist)
          {
             let parameters = this.distanceParameters.map(param => param.parameter);
             let index = parameters.indexOf(roadDist.parameter);      
             this.distanceFromRoad.setValue(this.distanceParameters[index]);
             this.distanceFromRoad.disable();
          }
          if(marketDist)
          {   
             let parameters = this.marketParameters.map(param => param.parameter);
             let index = parameters.indexOf(marketDist.parameter);         
             this.distanceFromMarket.setValue(this.marketParameters[index]);
             this.distanceFromMarket.disable();
          }

          if(roadDist || marketDist)
          {
          this.autoComplete = true;
          this.fetchType.setValue('Manually');
          this.fetchType.disable();

          this.isPlotOnRoad.setValue(isAdjToRoad);
          this.isPlotOnRoad.disable();

          this.marketValueMandatoryNotFilled= true;
          this.marketValueMandatoryNotFilledHint = 'Plot Location information is autofilled';
        }
        else{
          this.marketValueMandatoryNotFilled= true;
          this.marketValueMandatoryNotFilledHint = 'Plot Location information needs to be filled manually';
        }

        }  
    },
    err => {
      this.distanceFromRoad.enable();
      this.distanceFromMarket.enable();
      this.fetchType.enable();
      this.marketValueMandatoryNotFilled= true;
      this.marketValueMandatoryNotFilledHint = 'Plot Location information needs to be filled manually';
    }
  );
  
  }
}

decline()
{
  this.autoComplete = false;
  this.getMarketValue();
}

accept()
{
    this.autoComplete = false;
    this.marketValueMandatoryNotFilled = false;
    this.fetchType.enable();
    this.isPlotOnRoad.enable();
    this.distanceFromRoad.enable();
    this.distanceFromMarket.enable();
    this.fetchType.setValue('');
    this.isPlotOnRoad.setValue('');
    this.distanceFromRoad.setValue('');
    this.distanceFromMarket.setValue('');
}

setCurrentLandUse()
{
  if(this.districtCtrl.dirty && this.policeStationCtrl.dirty && this.mouzaCtrl.dirty
    && this.plotNo.dirty)
  {
    this.error = false;
  this.params = new Params(this.districtCtrl.value.districtName,
    this.policeStationCtrl.value.psName,this.mouzaCtrl.value.mouzaName,this.plotNo.value);
this.marketValueService.postMethodCallTextResponse('getCurrentLandUse',this.params).subscribe(
  res => {
    this.currLandUse.setValue(res);
    this.getBasePrice();
  },
  err => {
    this.error = true;
    this.errorMessage = 'Current Land use is not available for corresponding user selection ';
  }
);
  }
  else{
    let hint = '';
    if(!this.districtCtrl.dirty)
    {
       hint = hint + 'Select District;  ';
    }
    if(!this.policeStationCtrl.dirty)
    {
       hint = hint + 'Select Police Station;  ';
    }
    if(!this.mouzaCtrl.dirty)
    {
       hint = hint + 'Select Mouza';
    }

    if(!(hint==''))
    {
        this.basePriceMandatoryNotFilled = true;
        this.basePriceMandatoryNotFilledHint = hint;
    }
  }
}

  getBasePrice(){
    if(this.districtCtrl.dirty && this.policeStationCtrl.dirty && this.mouzaCtrl.dirty
      && this.plotNo.dirty)
    {
      this. error = false;
      this.basePriceMandatoryNotFilled = false;
      this.basePriceMandatoryNotFilledHint = '';
    this.params = new Params(this.districtCtrl.value.districtName,
      this.policeStationCtrl.value.psName,this.mouzaCtrl.value.mouzaName,this.plotNo.value);
    this.marketValueService.postMethodCall('getBasePrice',this.params).subscribe(
      res => {
        this.showBasePrice = true;
        this.basePrice = res;
        let basePrice =formatCurrency(Number(res),'en-IN',getCurrencySymbol('INR','narrow','en-IN'),'1.0-0');
        this.basePriceCtrl.setValue(basePrice);
      },
      err => {
        this.showBasePrice = false;
        this. error = true;
        this.errorMessage = 'No base value is available for corresponding user selection';
      }
    );
    }
    else{
      let hint = '';
      if(!this.districtCtrl.dirty)
      {
         hint = hint + 'Select District;  ';
      }
      if(!this.policeStationCtrl.dirty)
      {
         hint = hint + 'Select Police Station;  ';
      }
      if(!this.mouzaCtrl.dirty)
      {
         hint = hint + 'Select Mouza ';
      }

      if(!(hint==''))
      {
        this.basePriceMandatoryNotFilled = true;
        this.basePriceMandatoryNotFilledHint = hint;
      }

    }
  }
  onChangeAreaUnit()
  {
    this.areaOfPlot.enable();
  }

  onChangeArea()
  {
    this.getBasePriceMF();
  }

  getBasePriceMF(){
    if(this.districtCtrl.dirty && this.currLandUse.value && this.areaUnit.dirty
      && this.changeLandUseType.dirty && (this.changeLandUseType.value=='No' || (this.changeLandUseType.value=='Yes' && this.newLandType.dirty)) && this.basePriceCtrl.value)
    {
      this.error = false;
      this.basePriceMFMandatoryNotFilled = false;
      this.basePriceMFMandatoryNotFilledHint = '';
    this.params = new Params(this.districtCtrl.value.districtName,"","","",
    this.newLandType.value.landName,this.currLandUse.value,"","","",this.areaType.value,null,
      this.basePrice);
        this.marketValueService.postMethodCall('getBasePriceWithMF',this.params).subscribe(
      res => {
        this.showBasePriceMF = true;
        let basePriceWithMF= this.calculateBasedOnArea(Number(res));
        this.basePriceWithMF = basePriceWithMF;
        let formattedBasePriceMF =formatCurrency(Number(basePriceWithMF),'en-IN',getCurrencySymbol('INR','narrow','en-IN'),'1.0-0');
        this.basePriceMFCtrl.setValue(formattedBasePriceMF);
      },
      err => {
        this.showBasePriceMF = false;
        this.error = true;
        this.errorMessage = 'No base value with MF is available for corresponding user selection';

      }
    );
    }
    else{
      let hint = '';
      if(!this.districtCtrl.dirty)
      {
         hint = hint + 'Select District;  ';
      }
      if(!this.areaUnit.dirty)
      {
         hint = hint + 'Select Area Unit;  ';
      }
      if(!this.changeLandUseType.dirty)
      {
         hint = hint + 'Select Change Land Use Type;  ';
      }
      if(this.changeLandUseType.value=='Yes' && !this.newLandType.dirty)
      {
        hint = hint + 'Select New Land Use Type;  ';
      }
      if(this.basePriceCtrl.value == null || this.basePriceCtrl.value=='' )
      {
         hint = hint + 'Base Price is not avaiable  ';
      }

      if(!(hint==''))
      {
        this.basePriceMFMandatoryNotFilled = true;
        this.basePriceMFMandatoryNotFilledHint = hint;
      }

    }
  }

  getMarketValue(){
    if(this.districtCtrl.dirty && this.policeStationCtrl.dirty && this.mouzaCtrl.dirty
      && this.distanceFromRoad.value && this.distanceFromMarket.value &&
       this.areaType.value && this.basePriceMFCtrl.value )
    {
      this. error = false;
      this.marketValueMandatoryNotFilled = false;
      this.marketValueMandatoryNotFilledHint = '';
      let width = '';
      if(this.widthOfApproachRoad.value!=null)
      {
          width = this.widthOfApproachRoad.value.parameter;
      }
    this.params = new Params(this.districtCtrl.value.districtName,
      this.policeStationCtrl.value.psName,'','','','',this.distanceFromRoad.value.parameter,
      this.distanceFromMarket.value.parameter,width,this.areaType.value,this.transformToDecimal(),this.basePriceWithMF);
    this.marketValueService.postMethodCall('getMarketPrice',this.params).subscribe(
      res => {
        this.showMarketValue = true;
        this.marketPrice = res;
        let areaInDec = this.transformToDecimal();
        if(areaInDec>=1)
        {
          this.marketValuePerDecimal = Math.round(this.marketPrice / areaInDec);
        }
        else{
          this.marketValuePerDecimal = Math.round(this.marketPrice * areaInDec);
        }
        let formattedMarketValue =formatCurrency(Number(res),'en-IN',getCurrencySymbol('INR','narrow','en-IN'),'1.0-0');
        this.marketValueCtrl.setValue(formattedMarketValue);
      },
      err => {
        this.showMarketValue = false;
        this.error = true;
        this.errorMessage = 'No market value is available for corresponding user selection';

      }
    );
    }
    else{
      let hint = '';
      if(!this.districtCtrl.dirty)
      {
         hint = hint + 'Select District; ';
      }
      if(!this.policeStationCtrl.dirty)
      {
         hint = hint + 'Select Police Station; ';
      }
      if(!this.mouzaCtrl.dirty)
      {
         hint = hint + 'Select Mouza; ';
      }
      if(!this.distanceFromRoad.value)
      {
         hint = hint + 'Select Distance from road; ';
      }
      if(!this.distanceFromMarket.value)
      {
         hint = hint + 'Select Distance from Market; ';
      }
      if(!this.areaType.value)
      {
         hint = hint + 'Fill Area Type; ';
      }
      if(this.basePriceMFCtrl.value == null || this.basePriceMFCtrl.value=='' )
      {
         hint = hint + 'Base Price with MF is not avaiable';
      }
      

      if(!(hint==''))
      {
        this.marketValueMandatoryNotFilled = true;
        this.marketValueMandatoryNotFilledHint = hint;
      }

    }
  }

  transformToDecimal()
  {
    let area = this.areaOfPlot.value;
    if(this.areaUnit.value=='Decimal')
    {
        area = area * 1;
    }
    else if(this.areaUnit.value=='Acre')
    {
       area = area * 100;
    }
    else if(this.areaUnit.value=='Bigha')
    {
       area =  area * 33.05785123966942;
    }
    else if(this.areaUnit.value=='Katha')
    {
       area =  area * 1.652892561983471;
    }
    else if(this.areaUnit.value=='Chatak')
    {
       area = area * 0.10330578512396693;
    }
    else if(this.areaUnit.value=='Sq Feet')
    {
       area =  area * 0.00229167;
    }
    return Math.round(area);
  }


  calculateBasedOnArea(basePriceMF : number)
  {
    let finalBasePriceMF;
    let area = this.areaOfPlot.value;
    if(this.areaUnit.value=='Decimal')
    {
        finalBasePriceMF = basePriceMF * area;
    }
    else if(this.areaUnit.value=='Acre')
    {
       finalBasePriceMF = basePriceMF * area * 100;
    }
    else if(this.areaUnit.value=='Bigha')
    {
       finalBasePriceMF = basePriceMF * area * 33.05785123966942;
    }
    else if(this.areaUnit.value=='Katha')
    {
       finalBasePriceMF = basePriceMF * area * 1.652892561983471;
    }
    else if(this.areaUnit.value=='Chatak')
    {
       finalBasePriceMF = basePriceMF * area * 0.10330578512396693;
    }
    else if(this.areaUnit.value=='Sq Feet')
    {
       finalBasePriceMF = basePriceMF * area * 0.00229167;
    }
    return Math.round(finalBasePriceMF);
  }

  loadPrevMarketValue(){
    if(this.districtCtrl.dirty && this.policeStationCtrl.dirty && this.mouzaCtrl.dirty
      && this.plotNo.dirty && this.marketValueCtrl.value)
    {
      this.prevMarketVal = "";
      this.prevError = false;
      this.params = new Params(this.districtCtrl.value.districtName,
      this.policeStationCtrl.value.psName,this.mouzaCtrl.value.mouzaName,this.plotNo.value);  
      this.marketValueService.postMethodCall('getPrevMarketValue',this.params).subscribe(
      res => {
        this. error = false;
        this.prevMarketVal = res;
      },
      err => {
        this.prevMarketVal = "";
        this.prevError = true;
        this.prevErrorMessage = 'Previous transaction value is not available for corresponding user selection';
      }
    );
    }
    else{
      let hint = '';
      if(!this.districtCtrl.dirty)
      {
         hint = hint + 'Select District;  ';
      }
      if(!this.policeStationCtrl.dirty)
      {
         hint = hint + 'Select Police Station;  ';
      }
      if(!this.mouzaCtrl.dirty)
      {
         hint = hint + 'Select Mouza; ';
      }
      if(this.marketValueCtrl.value==null || this.marketValueCtrl.value=='')
      {
         hint = hint + 'Market Value is missing';
      }

      if(!(hint==''))
      {
        this.prevError = true;
        this.prevErrorMessage = hint;
      }

    }
  }

  loadDistanceParameters(){
    this.params = new Params(this.districtCtrl.value.districtName,
      this.policeStationCtrl.value.psName,this.mouzaCtrl.value.mouzaName);

    this.marketValueService.postMethodCall('getDistanceFromRoad',this.params).subscribe(
      res => {
        this.error = false;
        this.distanceParameters= res;
      },
      err => {
        this.error = true;
        this.errorMessage = 'Distance from road parameters could not be loaded';
      }
    );
  }

  loadMarketParameters(){
    this.params = new Params(this.districtCtrl.value.districtName,
      this.policeStationCtrl.value.psName,this.mouzaCtrl.value.mouzaName);

    this.marketValueService.postMethodCall('getDistanceFromMarket',this.params).subscribe(
      res => {
        this.error = false;
        this.marketParameters = res;
      },
      err => {
        this.error = true;
        this.errorMessage = 'Distance from Market parameters could not be loaded';
      }
    );
  }

  loadWidthParameters(){
    this.params = new Params(this.districtCtrl.value.districtName,
      this.policeStationCtrl.value.psName,this.mouzaCtrl.value.mouzaName);

    this.marketValueService.postMethodCall('getWidthOfRoad',this.params).subscribe(
      res => {
        this.error = false;
        this.widthParameters= res;
      },
      err => {
        this.error = true;
        this.errorMessage = 'Width of the road parameters could not be loaded';
      }
    );
  }

  onChangePlotAdjToRoad(event)
  {
    if(event.value == 'No' && this.areaType.value=='URBAN')
    {
       this.widthOfApproachRoad.enable();
       this.distanceFromRoad.enable();
       this.distanceFromRoad.setValue('');
    }
    else if(event.value == 'Yes')
    {
       let parameterTypes = this.distanceParameters.map(param => param.paramType);
       let index = parameterTypes.indexOf('MR0');
       this.distanceFromRoad.setValue(this.distanceParameters[index]);
       this.distanceFromRoad.disable();
       if(this.widthOfApproachRoad.value!=null)
       {
           this.widthOfApproachRoad.setValue('');
           this.widthOfApproachRoad.disable();
       }
       this.getMarketValue();
    }
    else if(event.value == 'No')
    {
      this.distanceFromRoad.enable();
      this.distanceFromRoad.setValue('');
    }

}

  onChangeLandUse(event)
  {
     if(event.value == 'Yes')
     {
          this.newLandType.enable();
          this.showBasePriceMF = false;
          this.loadLandTypes();
     }
     else{
      this.newLandType.disable();
      this.newLandType.setValue('');
     }
     this.OnChangeNewLandUse();
  }

  OnChangeNewLandUse()
  {
     if(this.areaOfPlot.dirty && this.areaUnit.dirty)
     {
         this.getBasePriceMF();
     }
  }

  onClickPreview(form : FormGroup)
  {
      sessionStorage.setItem('formPreview', JSON.stringify(form.getRawValue()));
      /*const url = this.router.serializeUrl(
        this.router.createUrlTree([`/form-preview`])
      );*/
      window.open(window.location.origin+'#/form-preview' , '_blank');
  }

  onClickDownloadPrevTransaction()
  {
    let httpParams = new HttpParams().set('responseType','blob');
    this.marketValueService.getPrevTx().subscribe(
    response => {
        const blob: any = new Blob([response], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        let link = document.createElement("a");
        if (link.download !== undefined) {
          this.error = false;
          let url = URL.createObjectURL(blob);
          link.setAttribute("href", url);
          link.setAttribute("download", "MarketValueTransactions");
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }
      },
      err => {
        this.error = true;
        this.errorMessage = 'Excel sheet could not be downloaded';
        window.scroll(0,0);
      }
    );
  }

  onClickSaveTransaction(form : FormGroup)
  {
    let formData= form.getRawValue();
    let width = ""; 
    if(formData.plotLocation.widthOfApproachRoad && formData.plotLocation.widthOfApproachRoad.parameter)
    {
       width = formData.plotLocation.widthOfApproachRoad.parameter;
    }
    
    let data = new Form(formData.jurisdictionInfo.district.districtName,formData.jurisdictionInfo.policeStation.psName,
      formData.jurisdictionInfo.mouza.mouzaName,formData.subJurisdiction.plotType.value,formData.subJurisdiction.plotNo,formData.subJurisdiction.bataNo,
      formData.subJurisdiction.khatianNo,formData.subJurisdiction.areaType,formData.subJurisdiction.currLandUse,
      this.basePrice,formData.landType.changeLandUseType,formData.landType.newLandType.landName,
      formData.landType.areaUnit,this.transformToDecimal(),this.basePriceWithMF,formData.plotLocation.fetchType,
      formData.plotLocation.isPlotOnRoad,formData.plotLocation.distanceFromRoad.parameter,width,
      formData.plotLocation.distanceFromMarket.parameter,this.marketPrice);
      console.log(data);
    this.marketValueService.postMethodCallTextResponse('formData',data).subscribe(
      res => {
        this. error = false;
        this.success = true;
        this.successMessage = "Data saved successfully!!";
        window.scroll(0,0);
      },
      err => {
        this.error = true;
        this.errorMessage = 'Form Data could not be saved';
        window.scroll(0,0);
      }
    );
  }

resetForm()
{
  this.jurisdictionForm.reset();
  this.success = false;
  this.error = false;
  this.prevError = false;
  this.marketValuePerDecimal = null;
}


}

