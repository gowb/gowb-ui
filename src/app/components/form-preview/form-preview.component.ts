import { Component, OnInit, OnDestroy } from '@angular/core';
import { MarketValueService } from 'src/app/services/marketvalue.service';
import { Form } from './../../model/Form';

@Component({
  selector: 'app-form-preview',
  templateUrl: './form-preview.component.html',
  styleUrls: ['./form-preview.component.css']
})
export class FormPreviewComponent implements OnInit, OnDestroy {

  public formData: any;
  public dataLoaded : boolean = false;
  title1 = "Jurisdiction Info";
  title2 = "Land Type";
  title3 = "Plot Location";


  constructor(private marketValueService: MarketValueService) { }

  ngOnInit(): void {
    
    JSON.parse(sessionStorage.getItem('formPreview')) ? 
    this.formData = JSON.parse(sessionStorage.getItem('formPreview')) :  null;
    if(this.formData!=null)
    {
       this.dataLoaded  = true;
    }
    console.log('formPreview',this.formData);
  }

  ngOnDestroy(){
    sessionStorage.removeItem('formPreview');
  }

  downloadForm(formData)
  {
     console.log(formData);

     let width = ""; 
    if(formData.plotLocation.widthOfApproachRoad && formData.plotLocation.widthOfApproachRoad.parameter)
    {
       width = formData.plotLocation.widthOfApproachRoad.parameter;
    }

     let data = new Form(formData.jurisdictionInfo.district.districtName,formData.jurisdictionInfo.policeStation.psName,
      formData.jurisdictionInfo.mouza.mouzaName,formData.subJurisdiction.plotType.value,formData.subJurisdiction.plotNo,formData.subJurisdiction.bataNo,
      formData.subJurisdiction.khatianNo,formData.subJurisdiction.areaType,formData.subJurisdiction.currLandUse,
      formData.subJurisdiction.basePrice,formData.landType.changeLandUseType,formData.landType.newLandType.landName,
      formData.landType.areaUnit,formData.landType.areaOfPlot,formData.landType.basePriceMF,formData.plotLocation.fetchType,
      formData.plotLocation.isPlotOnRoad,formData.plotLocation.distanceFromRoad.parameter,width,
      formData.plotLocation.distanceFromMarket.parameter,formData.plotLocation.marketValue);
      this.marketValueService.getPreviewPdf(data).subscribe(
      response => {
          console.log("Success");
          const blob: any = new Blob([response], { type: 'application/pdf' });
          let link = document.createElement("a");
          if (link.download !== undefined) {
            let url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", "MarketValueDetails");
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
          }
        },
        err => {
          console.log("Failure" + JSON.stringify(err));
        }
      );
  }


}