export class Form{
    district: String = '';
    policeStation: String = '';
    mouza: String = '';
    plotType: String = '';
    plotNumber: Number;
    bataNumber: Number;
    khatianNumber: Number;
    areaType: String = '';
    currLandUse: String = '';
    basePrice: Number;
    changeLandType: String = '';
    newLandUse: String = '';
    areaUnit: String = '';
    areaInDecimal: Number;
    basePriceMF: Number;
    fetchType: String = '';
    isPlotAdjToRoad: String = '';
    roadDistParameter: String = '';
    marketDistParameter: String = '';
    roadWidthParameter: String = '';
    marketValue: Number;

    constructor(district: string,policeStation?: string,mouza?: string,plotType?: string,
        plotNo?: Number,bataNumber?: Number,khatianNumber?: Number,  areaType?: string,
        currLandUse?: string,basePrice?: Number, changeLandType?: string, newLandUse?: string, areaUnit?: string,
        areaInDecimal?: Number, basePriceMF?: Number, fetchType?: string,isPlotAdjToRoad?: string,
        roadDistParameter?: string, roadWidthParameter?: string,marketDistParameter?: string,
        price?: Number){
        this.district = district;
        this.policeStation = policeStation;
        this.mouza = mouza;
        this.plotType = plotType;
        this.plotNumber = plotNo;
        this.bataNumber= bataNumber;
        this.khatianNumber= khatianNumber;
        this.areaType = areaType;
        this.currLandUse= currLandUse;
        this.basePrice = basePrice;
        this.changeLandType= changeLandType;
        this.newLandUse= newLandUse;
        this.areaUnit= areaUnit;
        this.areaInDecimal = areaInDecimal;
        this.basePriceMF = basePriceMF
        this.fetchType = fetchType;
        this.isPlotAdjToRoad = isPlotAdjToRoad;
        this.roadDistParameter = roadDistParameter;
        this.marketDistParameter = marketDistParameter;
        this.roadWidthParameter = roadWidthParameter;      
        this.marketValue = price;
    }

}