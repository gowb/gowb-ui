export class Params{
    district: String = '';
    policeStation: String = '';
    mouza: String = '';
    plotNo: String = '';
    landName: String = '';
    orgLandName: String = '';
    roadDistParameter: String = '';
    marketDistParameter: String = '';
    roadWidthParameter: String = '';
    areaType: String = '';
    areaInDecimal: Number;
    price: Number;

    constructor(district: string,policeStation?: string,mouza?: string,plotNo?: string,
        landName?: string, orgLandName?: string, roadDistParameter?: string,
        marketDistParameter?: string,roadWidthParameter?: string,
        areaType?: string, areaInDecimal?: Number, price?: Number){
        this.district = district;
        this.policeStation = policeStation;
        this.mouza = mouza;
        this.plotNo = plotNo;
        this.landName = landName;
        this.orgLandName = orgLandName;
        this.roadDistParameter = roadDistParameter;
        this.marketDistParameter = marketDistParameter;
        this.roadWidthParameter = roadWidthParameter;
        this.areaType = areaType;
        this.areaInDecimal = areaInDecimal;
        this.price = price;
    }

}