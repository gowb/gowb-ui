import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MarketValueService {

  serviceUrl: string =  environment.serviceUrl;

  constructor(private http : HttpClient) { }

  headerObj = new HttpHeaders()
          .set('Content-Type','application/json'); 

  httpOptions = { headers: this.headerObj };

  getMethodCall(methodToCall: string, params: HttpParams){
    return this.http.get(this.serviceUrl.concat(methodToCall),{params:params});
  }

  getPrevTx(){
    return this.http.get(this.serviceUrl.concat('getPrevTx'),{responseType : 'blob'});
  }

  getPreviewPdf(value: any){
    return this.http.post(this.serviceUrl.concat('downloadPdf'),value, {responseType : 'blob'});
  }

  postMethodCall(methodToCall: string, value: any){
    return this.http.post(this.serviceUrl.concat(methodToCall),JSON.stringify(value), this.httpOptions);
  }

  postMethodCallTextResponse(methodToCall: string, value: any){
    return this.http.post(this.serviceUrl.concat(methodToCall),JSON.stringify(value),{ headers: this.headerObj, responseType : 'text' });
  }

}
