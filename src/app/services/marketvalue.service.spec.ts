import { TestBed } from '@angular/core/testing';

import { MarketvalueService } from './marketvalue.service';

describe('MarketvalueService', () => {
  let service: MarketvalueService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MarketvalueService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
