import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { FormPreviewComponent } from './components/form-preview/form-preview.component';
import { JurisdictionComponent } from './components/jurisdiction/jurisdiction.component';


export const routes: Routes = [
  {
    path: 'form-preview',
    component: FormPreviewComponent
  },
  {
    path: 'gowb',
    component: JurisdictionComponent
  },
  {
    path: '',
    redirectTo: '/gowb',
    pathMatch: 'full'
  }
];
